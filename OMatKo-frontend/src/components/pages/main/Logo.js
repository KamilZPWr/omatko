import React, { Component } from 'react';
import logoPwr from '../../../images/logo_pwr.png';

export default class Logo extends Component {
    render() {
        return (
            <img className={this.props.style} src={logoPwr} alt="Logo Politechniki Wrocławskiej" />
        );
    }
}
