import React, { Component } from 'react';
import styles from '../../../styles/components/MainPage.module.scss';

export default class Text extends Component {
    render() {
        return (
            <div className={styles["main-text"]}>
                <h1>OMatKo!!! 2019</h1>
                <h2>12-14 kwietnia</h2>
                <article>
                    <br />
                    <h3>O KONFERENCJI</h3>
                    <p>
                        Ogólnopolska Matematyczna Konferencja Studentów "OMatKo!!!" powstała z inicjatywy studentów matematyki Politechniki Wrocławskiej
                        zrzeszonych w czterech kołach naukowych i przeznaczona jest dla studentów matematyki i kierunków pokrewnych z całego kraju.
                    </p>
                    <br />
                    <h3>Dlaczego warto być z nami?</h3>
                    <p>
                        Celem konferencji jest rozwój naukowy uczestników poprzez możliwość prezentacji interesujących zagadnień,
                         wyników pierwszych badań czy dzielenie się pasją z innymi studentami. Tegoroczna edycja "OMatKo!!!"
                          z pewnością będzie źródłem wielu inspiracji oraz przestrzenią do zawiązywania nowych kontaktów między ambitnymi młodymi ludźmi z różnych uczelni >>
                        Serdecznie zapraszamy!
                    </p>
                </article>
            </div>
        )
    }
}