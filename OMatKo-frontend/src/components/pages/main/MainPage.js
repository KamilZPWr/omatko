import React, { Component } from 'react';

import Layout from '../../layout/Layout';
import Carousel from './Carousel';
import Text from './Text';
import Logo from './Logo';
import styles from '../../../styles/components/MainPage.module.scss';


const MAIN_URL = `http://${window.location.hostname}:8000`;




export default class MainPage extends Component {

    state = {
        carouselImages: [],
        isLoading: true
    }

    componentDidMount() {
        fetch(`${MAIN_URL}/api/photos-picker/get`)
            .then(response => response.json())
            .then(photos => {
                this.setState({
                    carouselImages: photos,
                    isLoading: false
                });
            }
            );
    }

    render() {
        return (
            <main className={styles["main"]}>
                <Carousel style={styles["carousel"]} slideStyle={styles["slide"]} images={this.state.carouselImages} isLoading={this.state.isLoading} />
                <Text style={styles["main-text"]} />
                <Logo style={styles["logo"]} />
            </main>
        )
    }
}