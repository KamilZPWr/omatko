import React, { Component } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

import styles from '../../../styles/components/MainPage.module.scss';
import carouselImage1 from '../../../images/mockup/carousel_image1.png';


const MAIN_URL = `http://0.0.0.0:8000`;
const placeholder_image =
    [
        {
            src: carouselImage1,
            alt: "1"
        },
    ];


export default class Carousel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentImageIndex: 0
        };
    }

    componentDidMount() {
        this.imageChangeInterval = setInterval(() => {
            this.updateNextImage();
        }, 10000);
    }

    componentWillUnmount() {
        clearInterval(this.imageChangeInterval);
    }

    render() {
        return (
            this.props.isLoading ?
                <section className={styles.carousel}>
                    <TransitionGroup>
                        <CSSTransition
                            key={this.state.currentImageIndex}
                            timeout={10000}
                            classNames={{
                                appear: styles["fade-appear"],
                                appearActive: styles['fade-appear-active'],
                                enter: styles['fade-enter'],
                                enterActive: styles['fade-enter-active'],
                                enterDone: styles['fade-done-enter'],
                                exit: styles['fade-exit'],
                                exitActive: styles['fade-exit-active'],
                                exitDone: styles['fade-exit-done'],
                            }}
                        >
                            <div className={styles["slide-wrapper"]}>
                                <img className={styles.slide} src={placeholder_image[0].src} alt="" />
                            </div>
                        </CSSTransition>
                    </TransitionGroup>
                </section>
                :
                <section className={styles.carousel}>
                    <TransitionGroup>
                        <CSSTransition
                            key={this.state.currentImageIndex}
                            timeout={10000}
                            classNames={{
                                appear: styles["fade-appear"],
                                appearActive: styles['fade-appear-active'],
                                enter: styles['fade-enter'],
                                enterActive: styles['fade-enter-active'],
                                enterDone: styles['fade-done-enter'],
                                exit: styles['fade-exit'],
                                exitActive: styles['fade-exit-active'],
                                exitDone: styles['fade-exit-done'],
                            }}
                        >
                            <div className={styles["slide-wrapper"]}>
                                <img className={styles.slide} src={MAIN_URL + this.props.images[this.state.currentImageIndex].image} alt="" />
                            </div>
                        </CSSTransition>
                    </TransitionGroup>
                </section>
        )
    }

    updateNextImage() {
        let nextImageIndex = (this.state.currentImageIndex + 1) % this.props.images.length;

        this.setState({
            currentImageIndex: nextImageIndex
        });
    }
}