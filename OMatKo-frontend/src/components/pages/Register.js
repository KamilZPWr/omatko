import React from 'react'
import styles from '../../styles/components/Register.module.scss';

export default function Register() {
  return (
    <main>
      <div className={styles["register-closed"]}>
        <div>
          <header>
            <h1>
              Rejestracja
            </h1>
          </header>
          <p>
            Formularz zapisowy na "OMatKo!!!" 2019 został <span className={styles["closed"]}>zamknięty</span>.
         </p>
          <p>Bardzo dziękujemy wszystkim zapisanym i nie możemy się doczekać <b>12 kwietnia</b>. Do zobaczenia!</p>
        </div>

      </div>

    </main>
  )
}
