import React, { Component } from 'react'
import styles from '../../../styles/components/Schedule/AppliedMath.module.scss';


const MAIN_URL = `http://0.0.0.0:8000/`;

console.log(MAIN_URL);



export default class AppliedMath extends Component {

    constructor() {
        super();
        this.state = {
            schedule: [],
            isLoding: true,
        }

        this.toggleDescription = this.toggleDescription.bind(this);
        this.fetchDaySchedule = this.fetchDaySchedule.bind(this);
    }

    toggleDescription(event) {
        const description = event.currentTarget.parentElement.getElementsByClassName(styles["description"])[0];
        const icon = event.currentTarget.children[0];
        console.log(icon);
        description.classList.toggle(styles["active"]);
        icon.classList.toggle("fa-angle-down");
        icon.classList.toggle("fa-angle-up");

    }



    fetchDaySchedule(dayNum, tempSchedule) {
        const DAY_NAMES = {
            0: "Niedziela",
            1: "Poniedziałek",
            2: "Wtorek",
            3: "Środa",
            4: "Czwartek",
            5: "Piątek",
            6: "Sobota"
        }
        const nameDay = DAY_NAMES[dayNum];

        Promise.all([
            fetch(`${MAIN_URL}api/schedule/APP/${dayNum}/get`),
            fetch(`${MAIN_URL}api/schedule/OTHER/${dayNum}/get`)
        ])
            .then(([res1, res2]) => Promise.all([res1.json(), res2.json()])
                .then(([appliedMathLectures, other]) => {
                    let day = {};
                    day.weekday = nameDay;
                    day.schedule = [...appliedMathLectures, ...other];
                    day.schedule.sort(compareLectures);
                    tempSchedule.push(day);
                    tempSchedule.sort(compareDays);
                    //console.log(tempSchedule);
                    this.setState({
                        schedule: tempSchedule,
                    });
                }));



    }

    getTime(date) {
        const d = new Date(date);
        let hours = d.getHours();
        let minutes = d.getMinutes() > 9 ? d.getMinutes() : '0' + d.getMinutes();
        return hours + ":" + minutes;
    }


    componentDidMount() {
        let schedule = [];
        // 0 - niedziela 1 - poniedzialek ...

        this.fetchDaySchedule(5, schedule);
        this.fetchDaySchedule(6, schedule);
        this.fetchDaySchedule(0, schedule);

        this.setState({
            schedule,
            isLoding: false
        });
    }

    render() {
        return (
            this.state.isLoding ?
                <>
                    <h2>MATEMATYKA STOSOWANA</h2>
                    <p>Ładuję</p>
                </>
                :
                <>
                    <h2>MATEMATYKA STOSOWANA</h2>
                    <div className={styles["schedule"]}>
                        {
                            this.state.schedule.map(
                                (day, key) =>
                                    <div className={styles["day"]} key={key}>
                                        <div className={styles["weekday"]}>
                                            <h3>{day.weekday}</h3>
                                        </div>
                                        {day.schedule.map(
                                            (lecture, key) =>
                                                <div className={styles["lecture"]} key={key}>

                                                    <h4>{lecture.title}</h4>
                                                    <p>
                                                        {this.getTime(lecture.start_date)}
                                                        -
                                                        {this.getTime(lecture.end_date)}
                                                    </p>
                                                    <p>{lecture.presenter}</p>
                                                    <p className={styles["description"]}>{lecture.description}</p>
                                                    <div className={styles["more-info"]} onClick={this.toggleDescription}>
                                                        <i className='fas fa-angle-down'></i>
                                                    </div>

                                                </div>
                                        )}
                                    </div>
                            )
                        }
                    </div>

                </>
        )
    }


}

function compareDays(weekday1, weekday2) {
    const date1 = new Date(weekday1.schedule[0].start_date);
    const date2 = new Date(weekday2.schedule[0].start_date);

    if (date1 < date2)
        return -1;
    if (date1 > date2)
        return 1;
    return 0;
}

function compareLectures(lecture1, lecture2) {

    if (lecture1["start_date"] < lecture2["start_date"])
        return -1;
    if (lecture1["start_date"] > lecture2["start_date"])
        return 1;
    return 0;
}