import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

import styles from '../../../styles/components/Schedule/Schedule.module.scss';

import InnerNavbar from '../InnerNavbar';

import AppliedMath from './AppliedMath';
import TheoreticalMath from './TheoreticalMath';




const routes = [
    {
        destination: '/harmonogram/matematyka-teoretyczna',
        component: TheoreticalMath,
        content: "TEORETYCZNA",
        iconClassName: "fas fa-university"
    },
    {
        destination: '/harmonogram/matematyka-stosowana',
        component: AppliedMath,
        content: "STOSOWANA",
        iconClassName: "fa fa-calculator"
    },
]




export default class SchedulePage extends Component {

    render() {
        return (
            <main className={styles["main"]}>
                <InnerNavbar navLinks={routes} />
                <section className={styles["content"]}>
                    <Route
                        exact path="/harmonogram"
                        render={() =>
                            <Redirect to="/harmonogram/matematyka-teoretyczna" />
                        }
                    />
                    {
                        routes.map(({ destination, component }, key) =>
                            <Route key={key} path={destination} component={component} />
                        )
                    }
                </section>
            </main>


        )
    }
}