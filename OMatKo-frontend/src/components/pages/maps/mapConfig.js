const mapConfig = {
    INITIAL_LATITUDE: 51.109317,
    INITIAL_LONGITUDE: 17.058742,
    INITIAL_ZOOM: 16,
    API_KEY: "AIzaSyCIL9rW4YXXC6UQxOnat0uY6xHwCZVFJ0E",
    POINTS_DATA_ENDPOINT: "/api/map/all/get",
    POINTS: {
        "FD": {
            name: "Jedzenie",
            markerUrl: "http://maps.google.com/mapfiles/ms/icons/green-dot.png",
        },
        "ME": {
            name: "Zwiedzanie",
            markerUrl: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png",
        },
        "PT": {
            name: "Transport",
            markerUrl: "http://maps.google.com/mapfiles/ms/icons/red-dot.png",
        },
        "SS": {
            name: "Sklepy",
            markerUrl: "http://maps.google.com/mapfiles/ms/icons/pink-dot.png",
        },
        "HS": {
            name: "Hotele",
            markerUrl: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
        }
    },
};

export default mapConfig;
