import React, { Component } from "react";
import styles from "../../../styles/components/Map/Map.module.scss";
import mapConfig from "./mapConfig";

const MAIN_URL = '0.0.0.0:8000';

class Map extends Component {
    constructor(props) {
        super(props);
        this.createMap = this.createMap.bind(this);
        this.prepareScript = this.prepareScript.bind(this);
        this.updateState = this.updateState.bind(this);
    }

    componentDidMount() {
        if (!window.google) {
            this.prepareScript();
        } else {
            this.createMap();
        }

        fetch(MAIN_URL + mapConfig.POINTS_DATA_ENDPOINT)
            .then(pointsData => {
                return pointsData.json()
            })
            .then(pointsDataJson => {
                this.updateState({ pointsData: pointsDataJson });
            })
            .catch(err => {
                this.updateState({
                    pointsData: []
                });
            });
    }

    updateState(stateToAdd) {
        this.setState(previousState => {
            return Object.assign(
                {},
                previousState,
                stateToAdd
            )
        });
    }

    componentDidUpdate() {
        this.tryCreateMarkers();
    }

    tryCreateMarkers() {
        if ("pointsData" in this.state && "google" in window) {
            this.state.pointsData.forEach(point => {
                let markerIcon = mapConfig.POINTS[point.category].markerUrl;

                new window.google.maps.Marker({
                    map: this.state.map,
                    animation: window.google.maps.Animation.DROP,
                    position: { lng: point.longitude, lat: point.latitude },
                    icon: markerIcon
                });
            })
        }
    }

    prepareScript() {
        let s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "https://maps.google.com/maps/api/js?key=" + mapConfig.API_KEY;
        let x = document.getElementsByTagName("script")[0];
        x.parentNode.insertBefore(s, x);
        s.addEventListener("load", e => {
            this.createMap();
            this.createLegend();
        })
    }

    createMap() {
        let map = new window.google.maps.Map(
            document.getElementById(this.props.id),
            this.props.options
        );

        this.updateState({ map: map });
    }

    createLegend() {
        let legend = document.createElement("div");
        legend.setAttribute("id", "legend");

        for (let key in mapConfig.POINTS) {
            let type = mapConfig.POINTS[key].name;
            let name = mapConfig.POINTS[key].name;
            let icon = mapConfig.POINTS[key].markerUrl;
            let div = document.createElement('div');
            div.innerHTML = '<img src="' + icon + '"> ' + name;
            legend.appendChild(div);
        }

        let style = document.createElement('style');
        style.type = 'text/css';
        style.innerHTML = '#legend { background: #75baca;\n' +
            '  padding: 10px;\n' +
            '  margin: 10px;\n' +
            '  border: 2px dashed #fff; }\n' +
            '#legend img { vertical-align: middle; \n' +
            ' margin: 2px;}';
        document.getElementsByTagName('head')[0].appendChild(style);

        this.state.map.controls[window.google.maps.ControlPosition.LEFT_TOP].push(legend);
    }

    render() {
        return (
            <div className={styles["map"]} id={this.props.id} />
        );
    }
}
export default Map;