import React, { Component } from "react";
import Map from "./Map";
import styles from "../../../styles/components/Map/Map.module.scss"
import mapsPageConfig from "./mapsPageConfig";


export default class MapsPage extends Component {
    render() {
        return (
            <main className={styles["main"]}>
                <Map
                    id="map"
                    options={{
                        center: { lat: mapsPageConfig.INITIAL_LATITUDE, lng: mapsPageConfig.INITIAL_LONGITUDE },
                        zoom: mapsPageConfig.INITIAL_ZOOM
                    }}
                />
            </main>
        )
    }
}