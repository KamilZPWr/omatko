const mapsPageConfig = {
    INITIAL_LATITUDE: 51.109317,
    INITIAL_LONGITUDE: 17.058742,
    INITIAL_ZOOM: 16,
};

export default mapsPageConfig;
