import React, { useState, useEffect } from "react";
import axios from "axios";

import styles from "../../../styles/components/Vote/VotePage.module.scss";
import pwrLogo from "../../../images/logo_pwr.png";

const Vote = () => {
    const [user, setUser] = useState("");
    const [lecture, setLecture] = useState("");
    const [substantiveRating, setSubstantiveRating] = useState("3");
    const [presentationRating, setPresentationRating] = useState("3");
    const [error, setError] = useState("");
    const [success, setSuccess] = useState("");

    useEffect(() => {
        const token = localStorage.getItem("omatko-token");
        if (token) setUser(token);
    }, []);

    const submitForm = async e => {
        e.preventDefault();

        const content = Number(substantiveRating);
        const presentation = Number(presentationRating);

        try {
            const result = await axios.post(
                `http://0.0.0.0:8000/api/votes/${user}/${lecture}/${presentation}/${content}`
            );
            if (result.data) {
                localStorage.setItem("omatko-token", user);
                setSuccess("Głos został oddany pomyślnie!");
                setError("");
            }
        } catch (e) {
            setLecture("");
            setSuccess("");
            if (e.message === "Request failed with status code 409") {
                setError("Oddałeś już głos na ten wykład!");
            } else if (e.message === "Request failed with status code 400") {
                setError("Nieprawidłowy kod użytkownika lub wykładu!");
            }
        }
    };

    return (
        <div className={styles["vote"]}>
            <div className={styles["vote__blue-screen"]} />
            <div className={styles["vote__panel"]}>
                <h3 className={styles["vote__header"]}>Głosowanie</h3>

                <form className={styles["form"]} onSubmit={submitForm}>
                    <label className={styles["form__label"]} htmlFor="user-id">
                        Kod użytkownika
                    </label>
                    <input
                        className={styles["form__input"]}
                        type="text"
                        id="user-id"
                        name="user"
                        value={user}
                        required
                        onChange={e => {
                            setUser(e.target.value);
                        }}
                    />

                    <label className={styles["form__label"]} htmlFor="lecture">
                        Kod wykładu
                    </label>
                    <input
                        className={styles["form__input"]}
                        type="text"
                        id="lecture"
                        name="lecture"
                        value={lecture}
                        required
                        onChange={e => {
                            setLecture(e.target.value);
                        }}
                    />
                    {error && <div className={styles["error"]}>{error}</div>}
                    {success && (
                        <div className={styles["success"]}>{success}</div>
                    )}
                    <div className={styles["rating"]}>
                        <p className={styles["rating__desc"]}>
                            Ocena merytoryczna wykładu:{" "}
                        </p>
                        <div className={styles["flex-container"]}>
                            <div className={styles["radio__container"]}>
                                <input
                                    className={styles["radio"]}
                                    type="radio"
                                    value="1"
                                    name="substantiveRating"
                                    checked={substantiveRating === "1"}
                                    id="value_1"
                                    onChange={e =>
                                        setSubstantiveRating(e.target.value)
                                    }
                                />
                                <label
                                    className={styles["radio__label"]}
                                    htmlFor="value_1"
                                >
                                    1
                                </label>
                            </div>
                            <div className={styles["flex-container"]}>
                                <div className={styles["radio__container"]}>
                                    <input
                                        className={styles["radio"]}
                                        type="radio"
                                        value="2"
                                        name="substantiveRating"
                                        checked={substantiveRating === "2"}
                                        id="value_2"
                                        onChange={e =>
                                            setSubstantiveRating(e.target.value)
                                        }
                                    />
                                    <label
                                        className={styles["radio__label"]}
                                        htmlFor="value_2"
                                    >
                                        2
                                    </label>
                                </div>
                            </div>
                            <div className={styles["flex-container"]}>
                                <div className={styles["radio__container"]}>
                                    <input
                                        className={styles["radio"]}
                                        type="radio"
                                        value="3"
                                        name="substantiveRating"
                                        checked={substantiveRating === "3"}
                                        id="value_3"
                                        onChange={e =>
                                            setSubstantiveRating(e.target.value)
                                        }
                                    />
                                    <label
                                        className={styles["radio__label"]}
                                        htmlFor="value_3"
                                    >
                                        3
                                    </label>
                                </div>
                            </div>
                            <div className={styles["flex-container"]}>
                                <div className={styles["radio__container"]}>
                                    <input
                                        className={styles["radio"]}
                                        type="radio"
                                        value="4"
                                        name="substantiveRating"
                                        checked={substantiveRating === "4"}
                                        id="value_4"
                                        onChange={e =>
                                            setSubstantiveRating(e.target.value)
                                        }
                                    />
                                    <label
                                        className={styles["radio__label"]}
                                        htmlFor="value_4"
                                    >
                                        4
                                    </label>
                                </div>
                            </div>
                            <div className={styles["flex-container"]}>
                                <div className={styles["radio__container"]}>
                                    <input
                                        className={styles["radio"]}
                                        type="radio"
                                        value="5"
                                        name="substantiveRating"
                                        checked={substantiveRating === "5"}
                                        id="value_5"
                                        onChange={e =>
                                            setSubstantiveRating(e.target.value)
                                        }
                                    />
                                    <label
                                        className={styles["radio__label"]}
                                        htmlFor="value_5"
                                    >
                                        5
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={styles["rating"]}>
                        <p className={styles["rating__desc"]}>
                            Ocena prezentacji:{" "}
                        </p>
                        <div className={styles["flex-container"]}>
                            <div className={styles["radio__container"]}>
                                <input
                                    className={styles["radio"]}
                                    type="radio"
                                    value="1"
                                    name="presentation"
                                    checked={presentationRating === "1"}
                                    id="value_1"
                                    onChange={e =>
                                        setPresentationRating(e.target.value)
                                    }
                                />
                                <label
                                    className={styles["radio__label"]}
                                    htmlFor="value_1"
                                >
                                    1
                                </label>
                            </div>
                            <div className={styles["flex-container"]}>
                                <div className={styles["radio__container"]}>
                                    <input
                                        className={styles["radio"]}
                                        type="radio"
                                        value="2"
                                        name="presentation"
                                        checked={presentationRating === "2"}
                                        id="value_2"
                                        onChange={e =>
                                            setPresentationRating(
                                                e.target.value
                                            )
                                        }
                                    />
                                    <label
                                        className={styles["radio__label"]}
                                        htmlFor="value_2"
                                    >
                                        2
                                    </label>
                                </div>
                            </div>
                            <div className={styles["flex-container"]}>
                                <div className={styles["radio__container"]}>
                                    <input
                                        className={styles["radio"]}
                                        type="radio"
                                        value="3"
                                        name="presentation"
                                        checked={presentationRating === "3"}
                                        id="value_3"
                                        onChange={e =>
                                            setPresentationRating(
                                                e.target.value
                                            )
                                        }
                                    />
                                    <label
                                        className={styles["radio__label"]}
                                        htmlFor="value_3"
                                    >
                                        3
                                    </label>
                                </div>
                            </div>
                            <div className={styles["flex-container"]}>
                                <div className={styles["radio__container"]}>
                                    <input
                                        className={styles["radio"]}
                                        type="radio"
                                        value="4"
                                        name="presentation"
                                        checked={presentationRating === "4"}
                                        id="value_4"
                                        onChange={e =>
                                            setPresentationRating(
                                                e.target.value
                                            )
                                        }
                                    />
                                    <label
                                        className={styles["radio__label"]}
                                        htmlFor="value_4"
                                    >
                                        4
                                    </label>
                                </div>
                            </div>
                            <div className={styles["flex-container"]}>
                                <div className={styles["radio__container"]}>
                                    <input
                                        className={styles["radio"]}
                                        type="radio"
                                        value="5"
                                        name="presentation"
                                        checked={presentationRating === "5"}
                                        id="value_5"
                                        onChange={e =>
                                            setPresentationRating(
                                                e.target.value
                                            )
                                        }
                                    />
                                    <label
                                        className={styles["radio__label"]}
                                        htmlFor="value_5"
                                    >
                                        5
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button className={styles["button"]}>Głosuj</button>
                </form>
            </div>
            <img
                className={styles["logo"]}
                src={pwrLogo}
                alt="Logo Politechniki Wrocławskiej"
            />
        </div>
    );
};

export default Vote;
