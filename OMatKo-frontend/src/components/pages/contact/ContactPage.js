import React, { useEffect, useState } from "react";
import axios from "axios";
import { SingleContact } from "./SingleContact";
import styles from "../../../styles/components/Contact/ContactPage.module.scss";

import internetIcon from "../../../images/icons/internet.svg";
import mailIcon from "../../../images/icons/mail.svg";
import pwrLogo from "../../../images/logo_pwr.png";

const Contact = () => {
    const [data, setData] = useState(null);

    useEffect(() => {
        const fetchContacts = async () => {
            const { data } = await axios.get(
                "http://0.0.0.0:8000/api/contact/get"
            );
            setData(data);
        };
        fetchContacts();
    }, []);
    return (
        <div className={styles["contact"]}>
            <h2 className={styles["contact__header"]}>Kontakt</h2>
            <div className={styles["contact__emails"]}>
                <div className={styles["contact__email"]}>
                    <img
                        className={styles["contact__icon"]}
                        src={internetIcon}
                        alt="Adres strony internetowej"
                    />
                    <p className={styles["contact__content"]}>
                        omatko.im/pwr.wroc.pl
                    </p>
                </div>
                <div className={styles["contact__email"]}>
                    <img
                        className={styles["contact__icon"]}
                        src={mailIcon}
                        alt="Adres email"
                    />
                    <p className={styles["contact__content"]}>
                        omatko@pwr.wroc.pl
                    </p>
                </div>
            </div>
            <ul>
                {data &&
                    data.map(contact => (
                        <SingleContact key={contact.id} {...contact} />
                    ))}
            </ul>
            <img
                className={styles["logo"]}
                src={pwrLogo}
                alt="Logo Politechniki Wrocławskiej"
            />
        </div>
    );
};

export default Contact;
