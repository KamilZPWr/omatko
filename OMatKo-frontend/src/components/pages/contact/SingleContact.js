import React from "react";
import styles from "../../../styles/components/Contact/SingleContact.module.scss";

import mailIcon from "../../../images/icons/mail.svg";
import phoneIcon from "../../../images/icons/telefon.svg";

export const SingleContact = ({ photo, person, position, mail, phone }) => {
    return (
        <li className={styles["contact"]}>
            <img
                className={styles["contact__photo"]}
                src={`http://0.0.0.0:8000${photo}`}
                alt={"Zdjęcie " + person}
            />
            <div className={styles["contact__container"]}>
                <p className={styles["contact__name"]}>{person}</p>
                <p className={styles["contact__position"]}>{position}</p>
                <div className={styles["contact__div"]}>
                    <img
                        className={styles["contact__icon"]}
                        src={mailIcon}
                        alt="Adres strony internetowej"
                    />
                    <p className={styles["contact__mail"]}>{mail}</p>
                    <img
                        className={styles["contact__icon"]}
                        src={phoneIcon}
                        alt="Adres strony internetowej"
                    />
                    <p className={styles["contact__phone"]}>{phone}</p>
                </div>
            </div>
        </li>
    );
};
