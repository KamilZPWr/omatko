import React, { Component } from 'react';
import styles from '../../../styles/components/AboutConference/Organizers.module.scss';



const MAIN_URL = `http://0.0.0.0:8000`;

export default class Organizers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            organizers: [],
            isLoading: true
        }
    }

    componentDidMount() {

        fetch(`${MAIN_URL}/api/about/OR`)
            .then(response => response.json())
            .then(organizers => {
                this.setState({
                    organizers
                })
            });
    }

    render() {
        return (
            <>
                <h2>ORGANIZATORZY</h2>
                <div className={styles["organizers-list"]}>
                    {this.state.organizers.map((organizer, key) => {
                        return (
                            <a href={organizer.link} target="_blank" rel="noopener noreferrer" key={key}>
                                <div className={styles["organizers-image"]}>
                                    <img src={MAIN_URL + organizer.logo} alt={"Organizator"} />
                                </div>
                            </a>

                        )
                    })}
                </div>
            </>
        )
    }
}