import React, { Component } from 'react';
import styles from '../../../styles/components/AboutConference/Sponsors.module.scss';


const MAIN_URL = `http://0.0.0.0:8000`;

export default class Sponsors extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sponsors: [],
            isLoading: true
        }
    }

    componentDidMount() {

        fetch(`${MAIN_URL}/api/about/SP`)
            .then(response => response.json())
            .then(sponsors => {
                this.setState({
                    sponsors,
                    isLoading: false
                })
            });
    }

    render() {
        return (
            <>
                <h2>SPONSORZY</h2>

                <div className={styles["sponsor-list"]}>
                    {this.state.sponsors.map((sponsor, key) => {
                        return (
                            <a href={sponsor.link} target="_blank" rel="noopener noreferrer" key={key}>
                                <div className={styles["sponsor-image"]}>
                                    <img src={MAIN_URL + sponsor.logo} alt={"Sponsor"} />
                                </div>
                            </a>
                        )
                    })}
                </div>
            </>
        )
    }
}