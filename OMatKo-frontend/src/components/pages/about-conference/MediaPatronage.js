import React, { Component } from 'react';
import styles from '../../../styles/components/AboutConference/Patronage.module.scss';





const MAIN_URL = `http://0.0.0.0:8000`;


export default class MediaPatronage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mediaPatronage: [],
            isLoading: true
        }
    }

    componentDidMount() {

        fetch(`${MAIN_URL}/api/about/MP`)
            .then(response => response.json())
            .then(patronage => {
                this.setState({
                    mediaPatronage: patronage,
                    isLoading: false
                })
            });
    }

    render() {
        return (
            <>
                <h2>PATRONAT MEDIALNY</h2>
                <div className={styles["patronage-list"]}>
                    {this.state.mediaPatronage.map((patronage, key) => {
                        return (
                            <a href={patronage.link} target="_blank" rel="noopener noreferrer" key={key}>
                                <div className={styles["patronage-image"]}>
                                    <img src={MAIN_URL + patronage.logo} alt={"Patronat medialny"} />
                                </div>
                            </a>

                        )
                    })}
                </div>
            </>
        )
    }
}