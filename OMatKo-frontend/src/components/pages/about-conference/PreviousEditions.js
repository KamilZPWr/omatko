import React, { Component } from 'react';
import styles from '../../../styles/components/AboutConference/PreviousEditions.module.scss';

const MAIN_URL = `http://0.0.0.0:8000`;


export default class PreviousEditions extends Component {
    state = {
        editions: [],
        isLoading: true
    }

    componentDidMount() {
        fetch(`${MAIN_URL}/api/previous-editions/get`)
            .then(response => response.json())
            .then(previous => {
                this.setState({
                    editions: previous,
                    isLoading: false
                })
            })
    }

    getMonthName(val) {
        const MONTH_NAMES = {
            1: "stycznia",
            2: "lutego",
            3: "marca",
            4: "kwietnia",
            5: "maja",
            6: "czerwca",
            7: "lipca",
            8: "sierpnia",
            9: "września",
            10: "października",
            11: "listopada",
            12: "grudnia"
        }

        return MONTH_NAMES[val];
    }

    formatDate(date) {
        const d = new Date(date);
        let formatedDate = d.getDay() + "-" + (d.getDay() + 2);
        formatedDate += " " + this.getMonthName(d.getMonth());

        return formatedDate;
    }

    render() {
        return (
            this.state.isLoading ?
                <section>
                    <h2>POPRZEDNIE EDYCJE</h2>
                </section>
                :
                <section>
                    <h2>POPRZEDNIE EDYCJE</h2>
                    {
                        this.state.editions.map((edition, key) =>
                            <article className={styles["edition"]} key={key}>
                                <header>
                                    <h3>
                                        {this.formatDate(edition.date)}
                                    </h3>
                                </header>
                                <div className={styles["edition-content"]}>
                                    <p>{edition.content}</p>
                                    <img src={MAIN_URL + edition.image} alt=""></img>
                                </div>
                            </article>
                        )
                    }


                </section>
        )
    }
}