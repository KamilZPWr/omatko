import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

import styles from '../../../styles/components/AboutConference/AboutConferencePage.module.scss';

import InnerNavbar from '../InnerNavbar';

import Sponsors from './Sponsors';
import HonoraryPatronage from './HonoraryPatronage';
import MediaPatronage from './MediaPatronage';
import Organizers from './Organizers';
import PreviousEditions from './PreviousEditions';




const routes = [
    {
        destination: '/o-konferencji/sponsorzy',
        component: Sponsors,
        content: "SPONSORZY"
    },
    {
        destination: '/o-konferencji/patronat-honorowy',
        component: HonoraryPatronage,
        content: "PATRONAT HONOROWY"
    },
    {
        destination: '/o-konferencji/patronat-medialny',
        component: MediaPatronage,
        content: "PATRONAT MEDIALNY"
    },
    {
        destination: '/o-konferencji/organizatorzy',
        component: Organizers,
        content: "ORGANIZATORZY"
    },
    {
        destination: '/o-konferencji/poprzednie',
        component: PreviousEditions,
        content: "POPRZEDNIE EDYCJE"
    }
]

export default class AboutConferencePage extends Component {

    render() {
        return (
            <main className={styles["main"]}>
                <InnerNavbar navLinks={routes} />
                <section className={styles["content"]}>
                    <Route exact path="/o-konferencji" render={() =>
                        <Redirect to="/o-konferencji/sponsorzy" />
                    } />


                    {
                        routes.map(
                            ({ destination, component }, key) =>
                                <Route key={key} path={destination} component={component} />
                        )
                    }
                </section>
            </main>


        )
    }
}