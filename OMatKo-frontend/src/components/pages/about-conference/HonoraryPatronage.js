import React, { Component } from 'react';
import styles from '../../../styles/components/AboutConference/Patronage.module.scss';



const MAIN_URL = `http://0.0.0.0:8000`;


export default class HonoraryPatronage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            honoraryPatronage: []
        }
    }

    componentDidMount() {

        fetch(`${MAIN_URL}/api/about/HP`)
            .then(response => response.json())
            .then(patronage => {
                this.setState({
                    honoraryPatronage: patronage,
                    isLoading: false
                })
            });
    }
    render() {
        return (
            <>
                <h2>PATRONAT HONOROWY</h2>
                <div className={styles["patronage-list"]}>
                    {this.state.honoraryPatronage.map((patronage, key) => {
                        return (
                            <a href={patronage.link} target="_blank" rel="noopener noreferrer" key={key}>
                                <div className={styles["patronage-image"]}>
                                    <img src={MAIN_URL + patronage.logo} alt={"Patronat honorowy"} />
                                </div>
                            </a>
                        )
                    })}
                </div>
            </>
        )
    }
}