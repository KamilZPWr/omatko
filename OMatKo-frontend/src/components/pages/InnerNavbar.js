import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import styles from '../../styles/components/InnerNavbar.module.scss';


export default class Patronage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showInnerNav: true,
        }
        this.showInnerNav = this.showInnerNav.bind(this);
    }

    showInnerNav() {
        this.setState({
            showInnerNav: !this.state.showInnerNav
        });
    }

    render() {
        const ulStyles = this.state.showInnerNav ? styles["inner-nav-active"] : "hidden";
        const btnActive = this.state.showInnerNav ? styles["active"] : "hidden";

        return (
            <nav className={styles["inner-nav"]}>
                <ul className={ulStyles}>
                    {this.props.navLinks.map(({ destination, content, iconClassName }, key) => (
                        <li key={key}>
                            <NavLink to={destination} activeClassName={styles["navLink-active"]}>
                                <div className={styles["nav-item"]}>
                                    <i className={iconClassName}></i><span className={styles["nav-item-text"]}>{content}</span>
                                </div>
                            </NavLink>
                        </li>
                    ))}

                </ul>
                <div className={`${styles["inner-nav-btn"]} ${btnActive}`} onClick={this.showInnerNav}></div>
            </nav>
        )
    }
}