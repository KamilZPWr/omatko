import React, { useEffect, useState } from "react";
import axios from "axios";
import Carousel from "../main/Carousel";

import styles from "../../../styles/components/News.module.scss";
import stylesCarousel from "../../../styles/components/MainPage.module.scss";

import pwrLogo from "../../../images/logo_pwr.png";

const getImages = fetchedData => {
    const images = [];
    fetchedData.forEach(info => {
        images.push({
            src: `http://0.0.0.0:8000${info.image}`,
            alt: info.title
        });
    });

    return images;
};

const News = () => {
    const [data, setData] = useState(null);
    useEffect(() => {
        const getNews = async () => {
            const { data } = await axios.get(
                "http://0.0.0.0:8000/api/news/get"
            );
            setData(data);
        };
        getNews();
    }, []);

    return (
        <div className={styles["news"]}>
            {data && (
                <>
                    <div className={styles["news__carousel"]}>
                        <Carousel
                            style={stylesCarousel["carousel"]}
                            slideStyle={stylesCarousel["slide"]}
                            images={getImages(data)}
                        />
                    </div>
                    <div className={styles["news__container"]}>
                        <h3 className={styles["news__header"]}>Aktualności</h3>
                        <div className={styles["news__content"]}>
                            {data.map(info => (
                                <div
                                    className={styles["info"]}
                                    key={info.title}
                                >
                                    <p className={styles["info__date"]}>
                                        {info.date}
                                    </p>
                                    <h4 className={styles["info__title"]}>
                                        {info.title}
                                    </h4>
                                    <p className={styles["info__content"]}>
                                        {info.content}
                                    </p>
                                </div>
                            ))}
                        </div>
                    </div>
                </>
            )}
            <img
                className={styles["news__logo"]}
                src={pwrLogo}
                alt="Logo Politechniki Wrocławskiej"
            />
        </div>
    );
};

export default News;
