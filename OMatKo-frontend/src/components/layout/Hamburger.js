import React, { Component } from 'react';
import styles from '../../styles/components/Navbar.module.scss';
import NavLinkList from './NavLinkList';

export default class Hamburger extends Component {
    render() {
        return (
            <div className={this.props.className}>
                <div onClick={this.props.activate}>
                    <div className={this.props.iconClassName}></div>
                </div>
                <NavLinkList
                    className={`${styles["hamburger-menu"]}`}
                    navLinks={this.props.navLinks}
                    activeNavLink={`${styles["nav-link-active"]}`}
                />
            </div>
        )
    }
}