import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';


export default class NavLinkList extends Component {

    getClassNamesString(classNameArray) {
        let classNames = "";
        classNames = classNameArray.reduce((result, className) => result + " " + className);
        return classNames;
    }

    render() {
        const navLinks = this.props.navLinks;

        return (
            <ul className={this.props.className}>
                {
                    navLinks.map((navLink, key) => {
                        if (!navLink.isAnchor) {
                            return (
                                <li key={key}>
                                    <NavLink to={`/${navLink.destination}`} activeClassName={this.props.activeNavLink}>
                                        <div className={this.getClassNamesString(navLink.classNames)}>
                                            <img className={this.getClassNamesString(navLink.iconClassNames)} src={navLink.iconSrc} alt={navLink.iconAlt} />
                                            <span>{navLink.content}</span>
                                        </div>
                                    </NavLink>
                                </li>
                            )
                        } else {
                            return (
                                <li key={key}>
                                    <a href={navLink.destination} target="_blank" rel="noopener noreferrer">
                                        <div className={this.getClassNamesString(navLink.classNames)}>
                                            <img className={this.getClassNamesString(navLink.iconClassNames)} src={navLink.iconSrc} alt={navLink.iconAlt} />
                                            <span>{navLink.content}</span>
                                        </div>
                                    </a>
                                </li>
                            )
                        }


                    }

                    )
                }
            </ul>
        );
    }
}