import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styles from '../../styles/components/Header.module.scss';

export default class Header extends Component {
	render() {
		return (
			<header>
				<div className={styles['header-image-wrapper']}>
					<Link to="/">
						<div className={styles['header-image']}>
							{/* <img src={headerImg} alt="Logo oraz nazwa konferencji" /> */}
						</div>
					</Link>
				</div>

				{this.props.children}
			</header>
		);
	}
}

