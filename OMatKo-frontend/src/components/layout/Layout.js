import React, { Component } from 'react';
import Header from './Header';
import Navbar from './Navbar';
import Footer from './Footer';


export default class Layout extends Component {
    render() {
        return (
            <>
                <Header>
                    <Navbar />
                </Header>
                {this.props.children}
                <Footer />
            </>
        )
    }
}