import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import styles from '../../styles/components/Footer.module.scss';

import contactIcon from '../../images/icons/kontakt.svg';
import regulationIcon from '../../images/icons/regulamin.svg';
import facebookIcon from '../../images/icons/fb.svg';
import snapchatIcon from '../../images/icons/snapchat.svg';
import instagramIcon from '../../images/icons/instagram.svg';
import regulationUrl from '../../Regulamin_OMatKo_wersja1.pdf';
import offerIcon from '../../images/icons/oferta.svg'
import cooparationOfferUrl from '../../OMATKO_oferta_współpracy.pdf';

const REGULATION_URL = regulationUrl;
const FACEBOOK_URL = 'https://web.facebook.com/omatkopwr/';
const INSTAGRAM_URL = 'https://www.instagram.com/omatko.pwr/?hl=pl';
const SNPACHAT_URL = 'https://www.snapchat.com/add/omatkopwr';

export default class Footer extends Component {
    render() {
        return (
            <footer className={styles.footer}>
                <div className={styles["more-info"]}>
                    <a href={cooparationOfferUrl} target="_blank" rel="noopener noreferrer">
                        <div className="btn">
                            <img className="icon" src={offerIcon} alt="" />
                            <span>Oferta współpracy</span>
                        </div>
                    </a>
                    <a href={REGULATION_URL} target="_blank" rel="noopener noreferrer">
                        <div className="btn">
                            <img className="icon" src={regulationIcon} alt="" />
                            <span>Regulamin</span>
                        </div>
                    </a>
                </div>
                <div className={styles["social-media"]}>
                    <a href={INSTAGRAM_URL} target="_blank" rel="noopener noreferrer">
                        <div className="btn">
                            <img className="icon" src={instagramIcon} alt="" />
                            <span>@omatko.pwr</span>
                        </div>
                    </a>
                    <a href={SNPACHAT_URL} target="_blank" rel="noopener noreferrer">
                        <div className="btn">
                            <img className="icon" src={snapchatIcon} alt="" />
                            <span>@omatkopwr</span>
                        </div>
                    </a>
                    <a href={FACEBOOK_URL} target="_blank" rel="noopener noreferrer">
                        <div className="btn">
                            <img className="icon" src={facebookIcon} alt="" />
                            <span>/omatkopwr</span>
                        </div>
                    </a>
                </div>
            </footer>
        )
    }
}