import React, { Component } from "react";
import styles from "../../styles/components/Navbar.module.scss";

import NavLinkList from "./NavLinkList";
import Hamburger from "./Hamburger";

import registrationIcon from "../../images/icons/rejestracja.svg";
import scheduleIcon from "../../images/icons/harmonogram.svg";
import voteIcon from "../../images/icons/glosowanie.svg";
import newsIcon from "../../images/icons/aktual.svg";
import infoIcon from "../../images/icons/o_konferencji.svg";

import mapIcon from "../../images/icons/mapa.svg";
import contactIcon from "../../images/icons/kontakt.svg";

import facebookIcon from "../../images/icons/fb.svg";
import snapchatIcon from "../../images/icons/snapchat.svg";
import instagramIcon from "../../images/icons/instagram.svg";

const INSTAGRAM_URL = "https://www.instagram.com/omatko.pwr/?hl=pl";
const SNPACHAT_URL = "https://www.snapchat.com/add/omatkopwr";
const FACEBOOK_URL = "https://web.facebook.com/omatkopwr/";

export default class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: document.body.clientWidth,
            active: false,
            navLinks: [
                {
                    destination: "rejestracja",
                    classNames: ["btn", "btn-primary"],
                    iconClassNames: ["icon"],
                    iconSrc: registrationIcon,
                    iconAlt: "",
                    content: "Rejestracja",
                    isAnchor: false
                },
                {
                    destination: "harmonogram",
                    classNames: ["btn", "btn-primary"],
                    iconClassNames: ["icon"],
                    iconSrc: scheduleIcon,
                    iconAlt: "",
                    content: "Harmonogram",
                    isAnchor: false
                },
                {
                    destination: "glosowanie",
                    classNames: ["btn", "btn-primary"],
                    iconClassNames: ["icon"],
                    iconSrc: voteIcon,
                    iconAlt: "",
                    content: "Głosowanie",
                    isAnchor: false
                },
                {
                    destination: "aktualnosci",
                    classNames: ["btn", "btn-primary"],
                    iconClassNames: ["icon"],
                    iconSrc: newsIcon,
                    iconAlt: "",
                    content: "Aktualności",
                    isAnchor: false
                },
                {
                    destination: "o-konferencji",
                    classNames: ["btn", "btn-primary"],
                    iconClassNames: ["icon"],
                    iconSrc: infoIcon,
                    iconAlt: "",
                    content: "O konferencji",
                    isAnchor: false
                },
                // {
                //     destination: "#",
                //     classNames: ["btn", "btn-primary"],
                //     iconClassNames: ["icon"],
                //     iconSrc: scientificCircleIcon,
                //     iconAlt: "",
                //     content: "Koła naukowe",
                //     isAnchor: false
                // },

                {
                    destination: "mapa",
                    classNames: ["btn", "btn-primary"],
                    iconClassNames: ["icon"],
                    iconSrc: mapIcon,
                    iconAlt: "",
                    content: "Mapa miejsc i wykładów",
                    isAnchor: false
                },
                {
                    destination: "kontakt",
                    classNames: ["btn", "btn-primary"],
                    iconClassNames: ["icon"],
                    iconSrc: contactIcon,
                    iconAlt: "",
                    content: "Kontakt",
                    isAnchor: false
                },
                // Tylko gdy < 481 width aby social media były w navbarze
                {
                    destination: INSTAGRAM_URL,
                    classNames: ["btn"],
                    iconClassNames: ["icon"],
                    iconSrc: instagramIcon,
                    iconAlt: "",
                    content: "",
                    isAnchor: true
                },
                {
                    destination: SNPACHAT_URL,
                    classNames: ["btn"],
                    iconClassNames: ["icon"],
                    iconSrc: snapchatIcon,
                    iconAlt: "",
                    content: "",
                    isAnchor: true
                },
                {
                    destination: FACEBOOK_URL,
                    classNames: ["btn"],
                    iconClassNames: ["icon"],
                    iconSrc: facebookIcon,
                    iconAlt: "",
                    content: "",
                    isAnchor: true
                }
            ]
        };
        this.activateHamburger = this.activateHamburger.bind(this);
        this.onresize = this.onresize.bind(this);
    }

    activateHamburger() {
        this.setState(currentState => {
            return {
                active: !currentState.active
            };
        });
    }
    onresize() {
        this.setState(currentState => {
            return {
                width: document.body.clientWidth
            };
        });
    }
    render() {
        const socialMediaCount = 3;
        const allNavLinksCount = this.state.navLinks.length;
        let hamburgerLinks = [];
        let navbarLinks = this.state.navLinks.slice(
            0,
            allNavLinksCount - socialMediaCount
        );

        let width = this.state.width;
        window.addEventListener("resize", this.onresize);

        if (width <= 484) {
            hamburgerLinks = this.state.navLinks.slice(
                0,
                allNavLinksCount - socialMediaCount
            );
            navbarLinks = this.state.navLinks.slice(
                allNavLinksCount - socialMediaCount,
                allNavLinksCount
            );
        } else if (width <= 900) {
            hamburgerLinks = this.state.navLinks.slice(
                2,
                allNavLinksCount - socialMediaCount
            );
            navbarLinks = this.state.navLinks.slice(0, 2);
        } else if (width <= 1200) {
            hamburgerLinks = this.state.navLinks.slice(
                5,
                allNavLinksCount - socialMediaCount
            );
            navbarLinks = this.state.navLinks.slice(0, 5);
        } else {
            navbarLinks = this.state.navLinks.slice(
                0,
                allNavLinksCount - socialMediaCount
            );
        }

        return (
            <nav className={styles.navbar}>
                <Hamburger
                    className={`btn btn-primary ${styles.hamburger} ${
                        this.state.active ? styles["active"] : ""
                    }`}
                    iconClassName={`${styles["hamburger-icon"]} ${
                        this.state.active ? styles["active"] : ""
                    }`}
                    activate={this.activateHamburger}
                    navLinks={hamburgerLinks}
                />
                <NavLinkList
                    className={`${styles["nav-link-list"]}`}
                    activeNavLink={`${styles["nav-link-active"]}`}
                    navLinks={navbarLinks}
                />
            </nav>
        );
    }
}
