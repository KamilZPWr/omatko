import React from 'react';
import styles from '../styles/components/Missing.module.scss';
import PageNotFound from '../images/fogg-page-not-found.png';
import { Link } from 'react-router-dom';

const Missing = () => {
    return (
        <>
            <div className={styles["missing"]}>
                <div className={styles["missing-content"]}>
                    <img src={PageNotFound} alt="404 error" />
                    <h1>Szukana strona nie istnieje.</h1>
                    <p>Wpisany adres jest niepoprawny lub strona została usunięta</p>

                    <Link to="/">Przejdź do strony głównej</Link>
                </div>
            </div>

        </>

    )
}

export default Missing;