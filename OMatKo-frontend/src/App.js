import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Layout from "./components/layout/Layout";

import MainPage from "./components/pages/main/MainPage";
import AboutConferencePage from "./components/pages/about-conference/AboutConferencePage";
import News from "./components/pages/news/News";

import SchedulePage from "./components/pages/schedule/SchedulePage";
import ContactPage from "./components/pages/contact/ContactPage";
import Register from "./components/pages/Register";
import VotePage from "./components/pages/voting/Voting";

import Missing from "./components/Missing";
import MapsPage from "./components/pages/maps/MapsPage";

class App extends Component {
    render() {
        return (
            <>
                <Router>
                    <div>
                        <Layout>
                            <Switch>
                                <Route exact path="/" component={MainPage} />
                                <Route
                                    path="/rejestracja"
                                    component={Register}
                                />
                                <Route
                                    path="/kontakt"
                                    component={ContactPage}
                                />
                                <Route
                                    path="/harmonogram"
                                    component={SchedulePage}
                                />
                                <Route
                                    path="/o-konferencji"
                                    component={AboutConferencePage}
                                />
                                <Route path="/aktualnosci" component={News} />
                                <Route path="/mapa" component={MapsPage} />
                                <Route
                                    path="/glosowanie"
                                    component={VotePage}
                                />
                                <Route component={() => <Missing />} />
                            </Switch>
                        </Layout>
                    </div>
                </Router>
            </>
        );
    }
}

export default App;
