## Developing new functionalities
###### Instructions for robust and reliable code

##### install requirements
```commandline
pip install -r requirements.txt
```
##### Stat new app
```commandline
python manage.py startapp <name_of_app>
```
##### Write some tests
`OMatKO/apps/yourapp/tests.py`
##### Run tests
```commandline
pytest -vv
flake8
```

##### Commit and push
```commandline
git commit -m "message here"
git push origin
```

##### Squash Commits
To ensure that MR will become one revertable package please get familiar with git rebase
