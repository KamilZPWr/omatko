# Generated by Django 2.1.7 on 2019-03-19 20:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('votes', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='voteweight',
            name='name',
            field=models.CharField(choices=[('CONTENT', 'CONTENT'), ('PRESENTATION', 'PRESENTATION')], max_length=20, primary_key=True, serialize=False),
        ),
    ]
