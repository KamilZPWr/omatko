from datetime import datetime
from django.test import TestCase
from rest_framework import status
from OMatKo.apps.schedule.models import Event, TEO
from OMatKo.apps.votes.views import USER_ID_NOT_FOUND, LECTURE_ID_NOT_FOUND, \
    VOTE_ALREADY_EXISTS, VOTE_ADDED
from .models import Voter, VoteWeight, CONTENT, PRESENTATION, Vote

ASCII = 'ascii'
TITLE_VALUE = 'Title 1'
DESCRIPTION_VALUE = 'Description 1'
PRESENTER_VALUE = 'Presenter 1'
LECTURE_CODE_VALUE = 'lecture1'
VOTER_CODE_VALUE = 'VOTER_CODE'
NOT_EXISTING_USER = 'XYZ'
NOT_EXISTING_LECTURE = 'XYZ'


class VotesTestCase(TestCase):
    def setUp(self):
        Voter.objects.create(
            code=VOTER_CODE_VALUE
        )
        VoteWeight.objects.create(
            name=CONTENT,
            value=0.5
        )
        VoteWeight.objects.create(
            name=PRESENTATION,
            value=0.5
        )
        Event.objects.create(
            lecture_code=LECTURE_CODE_VALUE,
            title=TITLE_VALUE,
            start_date=datetime(2019, 1, 1, 12, 0, 0),
            end_date=datetime(2019, 1, 1, 13, 0, 0),
            presenter=PRESENTER_VALUE,
            description=DESCRIPTION_VALUE,
            type=TEO
        )

    def test_user_not_found(self):
        response = self.client.post(
            f'/api/votes/{NOT_EXISTING_USER}/{LECTURE_CODE_VALUE}/1/1'
        )
        response_content = response.content.decode(ASCII).strip('\"')

        assert response.status_code == status.HTTP_400_BAD_REQUEST \
            and response_content == USER_ID_NOT_FOUND

    def test_lecture_not_found(self):
        response = self.client.post(
            f'/api/votes/{VOTER_CODE_VALUE}/{NOT_EXISTING_LECTURE}/1/1'
        )
        response_content = response.content.decode(ASCII).strip('\"')

        assert response.status_code == status.HTTP_400_BAD_REQUEST \
            and response_content == LECTURE_ID_NOT_FOUND

    def test_vote_already_exist(self):
        Vote.objects.create(
            voter=Voter.objects.get(code=VOTER_CODE_VALUE),
            lecture=Event.objects.get(title=TITLE_VALUE),
            content_vote=1,
            presentation_vote=1
        )
        response = self.client.post(
            f'/api/votes/{VOTER_CODE_VALUE}/{LECTURE_CODE_VALUE}/1/1'
        )
        response_content = response.content.decode(ASCII).strip('\"')

        assert response.status_code == status.HTTP_409_CONFLICT \
            and response_content == VOTE_ALREADY_EXISTS

    def test_successful_vote(self):
        response = self.client.post(
            f'/api/votes/{VOTER_CODE_VALUE}/{LECTURE_CODE_VALUE}/1/1'
        )
        response_content = response.content.decode(ASCII).strip('\"')

        assert response.status_code == status.HTTP_201_CREATED \
            and response_content == VOTE_ADDED
