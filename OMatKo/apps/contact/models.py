from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Contact(models.Model):
    photo = models.ImageField(blank=True, upload_to='images/contact')
    position = models.TextField(null=False, blank=False)
    person = models.TextField(null=False, blank=False)
    mail = models.EmailField(null=False, blank=False, unique=True)
    phone = PhoneNumberField(null=False, blank=False, unique=True)
