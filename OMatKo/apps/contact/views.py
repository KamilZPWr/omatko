from django.http import JsonResponse
from OMatKo.apps.contact.models import Contact
from OMatKo.apps.contact.serializers import ContactSerializer


def get_contact(request):
    serializer = ContactSerializer(Contact.objects.all(), many=True)
    return JsonResponse(serializer.data, safe=False)
