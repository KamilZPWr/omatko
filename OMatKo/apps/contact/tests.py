import json
from django.test import TestCase

from .models import Contact

ID_VALUE = 1
PHOTO_VALUE = None
POSITION_VALUE = "administrator"
PERSON_VALUE = "Name Surname"
MAIL_VALUE = "mail@mail.com"
PHONE_VALUE = "+48999999999"

PHOTO = "photo"
POSITION = "position"
PERSON = "person"
MAIL = "mail"
PHONE = "phone"
ID = "id"


class ContactTestCase(TestCase):
    def setUp(self):
        Contact.objects.create(
            photo=PHOTO_VALUE,
            position=POSITION_VALUE,
            person=PERSON_VALUE,
            mail=MAIL_VALUE,
            phone=PHONE_VALUE
        )

    def test_call_view_success(self):
        expected_content = [{
            ID: ID_VALUE,
            PHOTO: PHOTO_VALUE,
            POSITION: POSITION_VALUE,
            PERSON: PERSON_VALUE,
            MAIL: MAIL_VALUE,
            PHONE: PHONE_VALUE
        }]
        response = self.client.get('/api/contact/')
        response_content = json.loads(response.content.decode('utf-8'))

        assert response_content == expected_content
