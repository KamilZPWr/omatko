from django.contrib import admin
from OMatKo.apps.photos_picker.models import Photo


admin.site.register(Photo)
