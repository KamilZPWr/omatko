from django.http import JsonResponse
from OMatKo.apps.photos_picker.models import Photo
from OMatKo.apps.photos_picker.serializers import PhotoSerializer


def get_photo(request):
    serializer = PhotoSerializer(Photo.objects.all(), many=True)
    return JsonResponse(serializer.data, safe=False)
