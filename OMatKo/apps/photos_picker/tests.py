import json
from django.test import TestCase
from OMatKo.apps.photos_picker.models import Photo

IMAGE = 'image'
DESCRIPTION = 'description'
IMAGE_URL_VALUE = 'IMAGE_URL'
DESCRIPTION_VALUE = 'DESCRIPTION'


class PhotosPickerTestCase(TestCase):
    def setUp(self):
        Photo.objects.create(
            description=DESCRIPTION_VALUE,
            image=IMAGE_URL_VALUE
        )

    def test_call_view_success(self):
        expected_content = [{
            DESCRIPTION: DESCRIPTION_VALUE,
            IMAGE: '/media/' + IMAGE_URL_VALUE
        }]
        response = self.client.get('/api/photos-picker/get')
        response_content = json.loads(response.content)
        assert response_content == expected_content
