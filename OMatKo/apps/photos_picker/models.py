from django.db import models


class Photo(models.Model):
    description = models.CharField(blank=False, null=False, max_length=150)
    image = models.ImageField(blank=False, null=False,
                              upload_to='images/photos_picker')
