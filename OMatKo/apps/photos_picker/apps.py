from django.apps import AppConfig


class PhotosPickerConfig(AppConfig):
    name = 'photos_picker'
