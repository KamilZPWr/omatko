from rest_framework import serializers
from OMatKo.apps.photos_picker.models import Photo


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = ('description', 'image')
