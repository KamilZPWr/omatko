from django.db import models

SPONSOR = 'sponsor'
HONORARY_PATRONAGE = 'honorary_patronage'
MEDIA_PATRONAGE = 'media_patronage'
ORGANIZER = 'organizer'
ALL = 'all'
TYPE = (
    ('SR', SPONSOR),
    ('HP', HONORARY_PATRONAGE),
    ('MP', MEDIA_PATRONAGE),
    ('OR', ORGANIZER)
)


class About(models.Model):
    logo = models.ImageField(blank=True, upload_to='images/about')
    link = models.URLField(max_length=250)
    type = models.CharField(
        blank=False,
        choices=TYPE,
        max_length=20
    )
