import json
from django.test import TestCase

from .models import About, SPONSOR, HONORARY_PATRONAGE, ALL

LOGO = 'logo'
LINK = 'link'
TYPE = 'type'
LOGO_1 = None
LOGO_2 = None
LINK_1 = 'https://www.link1.com/'
LINK_2 = 'https://www.link2.com/'


class AboutTestCase(TestCase):
    def setUp(self):
        About.objects.create(
            logo=LOGO_1,
            link=LINK_1,
            type=SPONSOR
        )
        About.objects.create(
            logo=LOGO_2,
            link=LINK_2,
            type=HONORARY_PATRONAGE
        )

    def test_get_all_types_success(self):
        expected_content = [
            {
                LOGO: LOGO_1,
                LINK: LINK_1,
                TYPE: SPONSOR
            },
            {
                LOGO: LOGO_2,
                LINK: LINK_2,
                TYPE: HONORARY_PATRONAGE
            }
        ]

        response = self.client.get(F'/api/about/{ALL}/')
        response_content = json.loads(response.content)

        assert expected_content == response_content

    def test_get_one_type_success(self):
        expected_content = [
            {
                LOGO: LOGO_1,
                LINK: LINK_1,
                TYPE: SPONSOR
            }
        ]

        response = self.client.get(F'/api/about/{SPONSOR}/')
        response_content = json.loads(response.content)

        assert expected_content == response_content
