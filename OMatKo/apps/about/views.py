from django.http import JsonResponse

from .models import About
from .serializers import AboutSerializer


def get_about(request, type):
    objects = About.objects.all()
    if type != 'all':
        objects = objects.filter(type=type)

    serializer = AboutSerializer(objects, many=True)

    return JsonResponse(serializer.data, safe=False)
