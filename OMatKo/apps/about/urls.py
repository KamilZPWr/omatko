from django.urls import path

from . import views

urlpatterns = [
    path(r'<str:type>/', views.get_about, name='get'),
]
