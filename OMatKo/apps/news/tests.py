import json
from django.test import TestCase

from .models import News

ID_VALUE = 1
IMAGE_VALUE = None
DATE_VALUE = "2019-01-01"
CONTENT_VALUE = "Content of first news."
TITLE_VALUE = "News 1"
IMAGE = "image"
DATE = "date"
CONTENT = "content"
TITLE = "title"
ID = "id"


class NewsTestCase(TestCase):
    def setUp(self):
        News.objects.create(
            title=TITLE_VALUE,
            content=CONTENT_VALUE,
            date=DATE_VALUE
        )

    def test_call_view_success(self):
        expected_content = [{
            ID: ID_VALUE,
            TITLE: TITLE_VALUE,
            CONTENT: CONTENT_VALUE,
            DATE: DATE_VALUE,
            IMAGE: IMAGE_VALUE
        }]
        response = self.client.get('/api/news/get')
        response_content = json.loads(response.content)

        assert response_content == expected_content
