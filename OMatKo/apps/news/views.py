from django.http import JsonResponse

from .models import News
from .serializers import NewsSerializer


def get_news(request):
    serializer = NewsSerializer(News.objects.all(), many=True)
    return JsonResponse(serializer.data, safe=False)
