from django.db import models


class News(models.Model):
    title = models.CharField(max_length=100, blank=False)
    content = models.TextField(null=False, blank=False)
    date = models.DateField(null=False, blank=False)
    image = models.ImageField(blank=True, upload_to='images/news')
