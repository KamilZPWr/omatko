from django.apps import AppConfig


class PreviousEditionsConfig(AppConfig):
    name = 'previous_editions'
