from rest_framework import serializers

from .models import Edition


class EditionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Edition
        fields = '__all__'
