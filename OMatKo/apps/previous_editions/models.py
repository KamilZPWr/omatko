from django.db import models


class Edition(models.Model):
    date = models.DateField(blank=False, null=False)
    content = models.TextField(blank=False, null=False)
    image = models.ImageField(blank=True, upload_to='images/previous_editions')
