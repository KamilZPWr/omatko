from django.http import JsonResponse

from .models import Edition
from .serializers import EditionsSerializer


def get_editions(request):
    serializer = EditionsSerializer(Edition.objects.all(), many=True)
    return JsonResponse(serializer.data, safe=False)
