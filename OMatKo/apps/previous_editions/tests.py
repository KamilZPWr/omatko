import json
from django.test import TestCase

from .models import Edition

IMAGE = 'image'
CONTENT = 'content'
DATE = 'date'
CONTENT_VALUE = 'First Edition description, it was really  nice!'
DATE_VALUE = '2019-01-01'
ID_VALUE = 1
ID = "id"


class EditionTestCase(TestCase):
    def setUp(self):
        Edition.objects.create(
            date=DATE_VALUE,
            content=CONTENT_VALUE,
            image=None
        )

    def test_call_view_success(self):
        expected_content = [{
            ID: ID_VALUE,
            DATE: DATE_VALUE,
            CONTENT: CONTENT_VALUE,
            IMAGE: None
        }]
        response = self.client.get('/api/previous-editions/get')
        response_content = json.loads(response.content)

        assert response_content == expected_content
