from random import randint
import numpy as np


def generate_code():
    numbers = [int(num) for num in np.linspace(48, 57, num=10)]

    lowercase = [int(num) for num in np.linspace(65, 72, num=8)] + \
                [int(num) for num in np.linspace(74, 90, num=17)]

    uppercase = [int(num) for num in np.linspace(97, 107, num=11)] +\
                [int(num) for num in np.linspace(109, 122, num=14)]

    chr_numbers = numbers + lowercase + uppercase

    return ''.join([chr(chr_numbers[randint(0, 59)]) for i in range(0, 20)])
