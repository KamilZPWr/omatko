from django.contrib import admin
from OMatKo.apps.schedule.models import Event

admin.site.register(Event)
