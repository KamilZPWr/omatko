import json
from datetime import datetime
from django.test import TestCase
from .models import Event, TEO, APP


DESCRIPTION_VALUE = 'DESCRIPTION'
PRESENTER_VALUE = 'PRESENTER'
DATE_4 = '2019-01-02T01:00:00Z'
DATE_3 = '2019-01-02T00:00:00Z'
DATE_2 = '2019-01-01T01:00:00Z'
DATE_1 = '2019-01-01T00:00:00Z'
TITLE_VALUE_1 = 'TITLE 1'
TITLE_VALUE_2 = 'TITLE 2'
TITLE_VALUE_3 = 'TITLE 3'
TITLE_VALUE_4 = 'TITLE 4'
TYPE = 'type'
DESCRIPTION = 'description'
PRESENTER = 'presenter'
END_DATE = 'end_date'
START_DATE = 'start_date'
TITLE = 'title'


class NewsTestCase(TestCase):
    def setUp(self):
        Event.objects.create(
            title=TITLE_VALUE_1,
            start_date=datetime(year=2019, month=1, day=1, hour=0),
            end_date=datetime(year=2019, month=1, day=1, hour=1),
            presenter=PRESENTER_VALUE,
            description=DESCRIPTION_VALUE,
            type=TEO,
            lecture_code='1'
        )

        Event.objects.create(
            title=TITLE_VALUE_2,
            start_date=datetime(year=2019, month=1, day=1, hour=0),
            end_date=datetime(year=2019, month=1, day=1, hour=1),
            presenter=PRESENTER_VALUE,
            description=DESCRIPTION_VALUE,
            type=APP,
            lecture_code='2'
        )

        Event.objects.create(
            title=TITLE_VALUE_3,
            start_date=datetime(year=2019, month=1, day=2, hour=0),
            end_date=datetime(year=2019, month=1, day=2, hour=1),
            presenter=PRESENTER_VALUE,
            description=DESCRIPTION_VALUE,
            type=TEO,
            lecture_code='3'
        )

        Event.objects.create(
            title=TITLE_VALUE_4,
            start_date=datetime(year=2019, month=1, day=2, hour=0),
            end_date=datetime(year=2019, month=1, day=2, hour=1),
            presenter=PRESENTER_VALUE,
            description=DESCRIPTION_VALUE,
            type=APP,
            lecture_code='4'
        )

    def test_weekday_call_view_success(self):
        expected_content = [
            {
                TITLE: TITLE_VALUE_1,
                START_DATE: DATE_1,
                END_DATE: DATE_2,
                PRESENTER: PRESENTER_VALUE,
                DESCRIPTION: DESCRIPTION_VALUE,
                TYPE: TEO
            },
            {
                TITLE: TITLE_VALUE_2,
                START_DATE: DATE_1,
                END_DATE: DATE_2,
                PRESENTER: PRESENTER_VALUE,
                DESCRIPTION: DESCRIPTION_VALUE,
                TYPE: APP
            }
        ]

        response = self.client.get(f'/api/schedule/2/get')
        response_content = json.loads(response.content)

        assert response_content == expected_content

    def test_type_call_view_success(self):
        expected_content = [
            {
                TITLE: TITLE_VALUE_1,
                START_DATE: DATE_1,
                END_DATE: DATE_2,
                PRESENTER: PRESENTER_VALUE,
                DESCRIPTION: DESCRIPTION_VALUE,
                TYPE: TEO
            },
            {
                TITLE: TITLE_VALUE_3,
                START_DATE: DATE_3,
                END_DATE: DATE_4,
                PRESENTER: PRESENTER_VALUE,
                DESCRIPTION: DESCRIPTION_VALUE,
                TYPE: TEO}
        ]

        response = self.client.get(f'/api/schedule/{TEO}/get')
        response_content = json.loads(response.content)

        assert response_content == expected_content

    def test_type_weekday_call_view_success(self):
        expected_content = [
            {
                TITLE: TITLE_VALUE_1,
                START_DATE: DATE_1,
                END_DATE: DATE_2,
                PRESENTER: PRESENTER_VALUE,
                DESCRIPTION: DESCRIPTION_VALUE,
                TYPE: TEO
            }
        ]

        response = self.client.get(f'/api/schedule/{TEO}/2/get')
        response_content = json.loads(response.content)
        assert response_content == expected_content

    def test_call_view_success(self):
        expected_content = [
            {
                TITLE: TITLE_VALUE_1,
                START_DATE: DATE_1,
                END_DATE: DATE_2,
                PRESENTER: PRESENTER_VALUE,
                DESCRIPTION: DESCRIPTION_VALUE,
                TYPE: TEO
            },
            {
                TITLE: TITLE_VALUE_2,
                START_DATE: DATE_1,
                END_DATE: DATE_2,
                PRESENTER: PRESENTER_VALUE,
                DESCRIPTION: DESCRIPTION_VALUE,
                TYPE: APP
            },
            {
                TITLE: TITLE_VALUE_3,
                START_DATE: DATE_3,
                END_DATE: DATE_4,
                PRESENTER: PRESENTER_VALUE,
                DESCRIPTION: DESCRIPTION_VALUE,
                TYPE: TEO
            },
            {
                TITLE: TITLE_VALUE_4,
                START_DATE: DATE_3,
                END_DATE: DATE_4,
                PRESENTER: PRESENTER_VALUE,
                DESCRIPTION: DESCRIPTION_VALUE,
                TYPE: APP
            }
        ]

        response = self.client.get(f'/api/schedule/get')
        response_content = json.loads(response.content)
        assert response_content == expected_content
