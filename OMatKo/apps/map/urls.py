from django.urls import path

from . import views

urlpatterns = [
    path(r'<str:category>/get', views.get_place, name='get'),
]
