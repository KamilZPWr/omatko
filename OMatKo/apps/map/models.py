from django.db import models

HOTELS = 'hotels'
SHOPS = 'shops'
PUBLIC_TRANSPORT = 'public_transport'
MUST_SEE = 'must_see'
FOOD = 'food'
ALL = 'all'
CATEGORY = (
    ('FD', FOOD),
    ('ME', MUST_SEE),
    ('PT', PUBLIC_TRANSPORT),
    ('SS', SHOPS),
    ('HS', HOTELS)
)


class Place(models.Model):
    longitude = models.FloatField(blank=False)
    latitude = models.FloatField(blank=False)
    name = models.CharField(max_length=80, blank=False)
    category = models.CharField(
        blank=False,
        choices=CATEGORY,
        max_length=20
    )

    class Meta:
        unique_together = (('longitude', 'latitude'),)
