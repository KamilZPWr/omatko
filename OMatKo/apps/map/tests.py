import json
from django.test import TestCase

from .models import Place, FOOD, MUST_SEE, ALL

LONGITUDE = 'longitude'
LATITUDE = 'latitude'
NAME = 'name'
CATEGORY = 'category'
PLACE_1 = 'Place 1'
PLACE_2 = 'Place 2'
LONGITUDE_VALUE_1 = 0.0
LATITUDE_VALUE_1 = 0.0
LONGITUDE_VALUE_2 = 0.1
LATITUDE_VALUE_2 = 0.1


class PalceTestCase(TestCase):
    def setUp(self):
        Place.objects.create(
            longitude=LONGITUDE_VALUE_1,
            latitude=LATITUDE_VALUE_1,
            name=PLACE_1,
            category=FOOD
        )
        Place.objects.create(
            longitude=LONGITUDE_VALUE_2,
            latitude=LATITUDE_VALUE_2,
            name=PLACE_2,
            category=MUST_SEE
        )

    def test_get_all_categories_success(self):
        expected_content = [
            {
                LONGITUDE: LONGITUDE_VALUE_1,
                LATITUDE: LATITUDE_VALUE_1,
                NAME: PLACE_1,
                CATEGORY: FOOD
            },
            {
                LONGITUDE: LONGITUDE_VALUE_2,
                LATITUDE: LATITUDE_VALUE_2,
                NAME: PLACE_2,
                CATEGORY: MUST_SEE
            }
        ]

        response = self.client.get(F'/api/map/{ALL}/get')
        response_content = json.loads(response.content)

        assert expected_content == response_content

    def test_get_one_category_success(self):
        expected_content = [
            {
                LONGITUDE: LONGITUDE_VALUE_1,
                LATITUDE: LATITUDE_VALUE_1,
                NAME: PLACE_1,
                CATEGORY: FOOD
            }
        ]

        response = self.client.get(F'/api/map/{FOOD}/get')
        response_content = json.loads(response.content)

        assert expected_content == response_content
