from django.http import JsonResponse

from .models import Place
from .serializers import PlaceSerializer


def get_place(request, category):
    objects = Place.objects.all()
    if category != 'all':
        objects = objects.filter(category=category)

    serializer = PlaceSerializer(objects, many=True)

    return JsonResponse(serializer.data, safe=False)
