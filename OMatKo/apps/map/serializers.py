from rest_framework import serializers

from .models import Place


class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = '__all__'

    def to_representation(self, instance):
        return {
            'longitude': instance.longitude,
            'latitude': instance.latitude,
            'name': instance.name,
            'category': instance.category
        }
