"""OMatKo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include

from django.conf import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'api/previous-editions',
        include('OMatKo.apps.previous_editions.urls')),
    url(r'api/news', include('OMatKo.apps.news.urls')),
    url(r'^api/map/', include('OMatKo.apps.map.urls')),
    url(r'^api/contact/', include('OMatKo.apps.contact.urls')),
    url(r'^api/photos-picker/', include('OMatKo.apps.photos_picker.urls')),
    url(r'^api/schedule/', include('OMatKo.apps.schedule.urls')),
    url(r'^api/about/', include('OMatKo.apps.about.urls')),
    url(r'^api/votes/', include('OMatKo.apps.votes.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
