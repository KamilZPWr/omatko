ALLOWED_HOSTS = ['www.omatko.pl']


DATABASES['default'] = {
    'ENGINE': 'django.db.backends.postgresql',
    'NAME': 'postgres',
    'USER': 'postgres',
    'HOST': os.environ.get('POSTGRES_HOST', 'db'),
    'PORT': 5432,
}