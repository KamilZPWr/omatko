from .base import *
import dj_database_url

DATABASE_URI = os.environ.get('DATABASE_URI', 'sqlite:///{}'.format(
    ROOT_DIR.path('db.sqlite3')))

DATABASES['default'] = dj_database_url.config(
    default=DATABASE_URI.replace('+pyodbc', ''))
