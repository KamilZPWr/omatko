import os


def set_default_settings_file():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                          "OMatKo.server.settings.local")
