# How to run backend?
### Using docker
1. Install [docker](https://docs.docker.com/cs-engine/1.13/).
2. Open terminal.
3. Build image by **docker-compose build**.
4. Run images **docker-compose up** or **docker-compose up -d** to hide details about runing containers.

### Using django
1. Go into **omatko** folder.
2. Activate virtualenv with **source ./venv/bin/activate**
2. Run migrations with **python manage.py migrate**.
3. Run server **python manage.py runserver**.

###### [docs](/docs) for backend developers